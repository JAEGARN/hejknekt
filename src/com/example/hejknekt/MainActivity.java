package com.example.hejknekt;

import java.util.Calendar;

import android.support.v7.app.ActionBarActivity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements android.view.View.OnClickListener {
	private Button playButton;
	private Button aboutButton;
	private TextView startText;
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else if(getResources().getBoolean(R.bool.landscape_only)){
        	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
     
        setAlarm();
        
        
        
        startText = (TextView) findViewById(R.id.start_text);
		startText.setTextSize(getResources().getDisplayMetrics().widthPixels/15);
		
        playButton = (Button) findViewById(R.id.play_button);
        playButton.setOnClickListener(this);
        
        aboutButton = (Button) findViewById(R.id.about_button);
        aboutButton.setOnClickListener(this);
    }
    
    @Override
	public void onClick(View v) {
    	if(playButton.isPressed()){
			Intent i = new Intent(getApplicationContext(), GameActivity.class);
			startActivity(i);
		}else if(aboutButton.isPressed()){
			Intent i = new Intent(getApplicationContext(), AboutActivity.class);
			startActivity(i);
		}
	}
    
    public void setAlarm(){
    	
    	PendingIntent pendingIntent;
		Calendar calendar = Calendar.getInstance();
         
		calendar.set(Calendar.HOUR_OF_DAY, 19);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.AM_PM,Calendar.PM);
        
		Intent myIntent = new Intent(MainActivity.this, MyReceiver.class);
		pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent,0);
        
		AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
		alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }
    
 
}
    
    


