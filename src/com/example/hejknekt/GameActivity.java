package com.example.hejknekt;

import com.example.TheGame.CardMissions;
import com.example.TheGame.GameEngine;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GameActivity extends ActionBarActivity implements android.view.View.OnClickListener {
	private Button drawButton;
	private GameEngine game;
	private TextView cardText, cardDescription;
	private CardMissions cm;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		
		if(getResources().getBoolean(R.bool.portrait_only)){
	        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }else if(getResources().getBoolean(R.bool.landscape_only)){
        	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
		
		game = new GameEngine();
		cm = new CardMissions();
		
		cardDescription = (TextView) findViewById(R.id.cards_description_text);
		cardDescription.setTextSize(getResources().getDisplayMetrics().widthPixels/70);
		
		cardText = (TextView) findViewById(R.id.cards_text);
		cardText.setTextSize(getResources().getDisplayMetrics().widthPixels/15);
		
		drawButton = (Button) findViewById(R.id.draw_button);
        drawButton.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		
		if(game.getCards().size() > 0){
			String card = game.drawCard();
			
			if(card.equals(cm.getAddRule())){
				cardDescription.setText(cm.getAddDes());
			}else if(card.equals(cm.getBrands())){
				cardDescription.setText(cm.getBrandsDes());
			}else if(card.equals(cm.getDeleteRule())){
				cardDescription.setText(cm.getDelDes());
			}else if(card.equals(cm.getGiveBeer()+" (1)")||card.equals(cm.getGiveBeer()+" (2)")||card.equals(cm.getGiveBeer()+" (3)")||card.equals(cm.getGiveBeer()+" (4)")||card.equals(cm.getGiveBeer()+" (5)")){
				cardDescription.setText(cm.getGiveDes());
			}else if(card.equals(cm.getHelloQueen())){
				cardDescription.setText(cm.getQueenDes());
			}else if(card.equals(cm.getHeyJack())){
				cardDescription.setText(cm.getJackDes());
			}else if(card.equals(cm.getPingPong())){
				cardDescription.setText(cm.getPingDes());
			}else if(card.equals(cm.getTakeBeer()+ " (1)")||card.equals(cm.getTakeBeer()+ " (2)")||card.equals(cm.getTakeBeer()+ " (3)")||card.equals(cm.getTakeBeer()+ " (4)")||card.equals(cm.getTakeBeer()+ " (5)")){
				cardDescription.setText(cm.getTakeDes());
			}else if(card.equals(cm.getTheElk())){
				cardDescription.setText(cm.getElkDes());
			}else if(card.equals(cm.getWaterfall())){
				cardDescription.setText(cm.getWaterDes());
			}else{
				cardDescription.setText("");
			}
			
			cardText.setText(card);
			game.getCards().remove(game.getTheCurrentNumber());
			
			drawButton.setEnabled(false);
			drawButton.postDelayed(new Runnable() {
			    @Override
			    public void run() {
			    	drawButton.setEnabled(true);
			    }
			}, 3000);
		}else{
			cardText.setText("Rundan är tyvärr slut");
			drawButton.setEnabled(false);
			drawButton.postDelayed(new Runnable() {
			    @Override
			    public void run() {
			    	drawButton.setEnabled(true);
			    	Intent i = new Intent(getApplicationContext(), MainActivity.class);
					startActivity(i);
			    }
			}, 4000);
			
		}
	}
}
