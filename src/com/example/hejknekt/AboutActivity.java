package com.example.hejknekt;

import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

public class AboutActivity extends ActionBarActivity implements android.view.View.OnClickListener {
	private TextView author;
	private Button homeButton, playButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		if(getResources().getBoolean(R.bool.portrait_only)){
	        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }else if(getResources().getBoolean(R.bool.landscape_only)){
        	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
		
		author = (TextView) findViewById(R.id.mail_text);
		author.setText(Html.fromHtml("<a href=\"mailto:viktor.jegeras@panceait.se\">Skicka feedback eller frågor</a>"));
		author.setMovementMethod(LinkMovementMethod.getInstance());
		
		homeButton = (Button) findViewById(R.id.home_button);
        homeButton.setOnClickListener(this);
        
        playButton = (Button) findViewById(R.id.play_button);
        playButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(playButton.isPressed()){
			Intent i = new Intent(getApplicationContext(), GameActivity.class);
			startActivity(i);
		}else if(homeButton.isPressed()){
			Intent i = new Intent(getApplicationContext(), MainActivity.class);
			startActivity(i);
		}
	}

	

	
}
