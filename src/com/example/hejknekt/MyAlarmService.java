package com.example.hejknekt;

import com.example.hejknekt.R;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
                            
 

@SuppressLint("NewApi")
public class MyAlarmService extends Service 
{
	@Override
    public IBinder onBind(Intent arg0)
    {
       // TODO Auto-generated method stub
        return null;
    }
 
    @Override
    public void onCreate() 
    {
    	super.onCreate();
    	showNotification();
    }
 
   @Override
   public void onStart(Intent intent, int startId) {
       super.onStart(intent, startId);
       //showNotification();
    }
 
    @Override
    public void onDestroy() 
    {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
    
    public void showNotification(){
    	Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

    	Intent intent1 = new Intent(this.getApplicationContext(),MainActivity.class);

    	PendingIntent pendingNotificationIntent = PendingIntent.getActivity( this.getApplicationContext(),0, intent1,PendingIntent.FLAG_UPDATE_CURRENT);

        Notification mNotification = new Notification.Builder(this)
        							.setContentTitle("Where are you?")
    								.setContentText("Come on let's play and get drunk!")
    								.setSmallIcon(R.drawable.hj)
    								.setContentIntent(pendingNotificationIntent)
    								.setSound(soundUri).build();
    								
		
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        
        if (Context.NOTIFICATION_SERVICE!=null) {
        	String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(ns);
            nMgr.cancel(0);
        }

        
    	mNotification.flags |= Notification.FLAG_AUTO_CANCEL;
    	notificationManager.notify(0, mNotification);
    	
    }

}
