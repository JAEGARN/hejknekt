package com.example.TheGame;

public class CardMissions {
	private String giveBeer, takeBeer, theElk, pingPong, waterfall, heyJack, helloQueen, brands, addRule, deleteRule;
	private String giveDes, takeDes, elkDes, pingDes, waterDes, jackDes, queenDes, brandsDes, addDes, delDes;
	public CardMissions(){
		giveBeer = "Ge bort klunkar";
		takeBeer = "Ta klunkar";
		theElk = "Älgen";
		pingPong = "Ping-Pong";
		waterfall = "Vattenfall";
		heyJack = "Hej Knekt";
		helloQueen = "Goddag Dam";
		brands = "Märken";
		addRule = "Lägg till regel";
		deleteRule = "Ta bort regel";
		
		takeDes = "Drick antal klunkar som står inom parantesen";
		giveDes = "Ge bort antal klunkar som står inom parantesen till en annan spelare";
		elkDes = "Spelaren som drar kortet sätter sina tummar mot tinningen och börjar vinka, spelaren till vänster gör samma med sin högra hand"
				+ " och spelaren till höger höjer sin vänstra hand. Man skickar sedan vidare älgen genom att peka på en spelare med bägge händerna."
				+ " Forsätt tills någon missar.";
		pingDes = "Spelaren börjar med att säga PING det betyder att man skickar vidare till nästa person och PONG betyder byt håll, fortsätt till någon missar.";
		waterDes = "Alla spelare börjar dricka samtidigt och man får inte sluta förrän den personen som sitter innan har slutat dricka. Spelaren som fick kortet ska avbryta först.";
		jackDes = "När Hej Knekt dras så ställer sig alla upp, gör honör och säger HEJ KNEKT! den som gör detta sist förlorar.";
		queenDes = "När Goddag Dam dras så ställer sig alla upp höjer sin högra hand och säger GODDAG DAM! den som gör detta sist förlorar";
		brandsDes = "Spelaren som drar kortet väljer ett märke, T.ex Bilmärken och börjar säga något, den som inte kan eller säger samma som någon tidigare förlorar";
		addDes = "Spelaren som drar kortet får välja en godtycklig regel som gäller tills någon tar bort den eller spelet är slut.";
		delDes = "Spelaren som drar kortet får ta bort valfri regel, finns det ingen så går turen vidare.";
	}

	public String getGiveDes() {
		return giveDes;
	}

	public String getTakeDes() {
		return takeDes;
	}

	public String getElkDes() {
		return elkDes;
	}

	public String getPingDes() {
		return pingDes;
	}

	public String getWaterDes() {
		return waterDes;
	}

	public String getJackDes() {
		return jackDes;
	}

	public String getQueenDes() {
		return queenDes;
	}

	public String getBrandsDes() {
		return brandsDes;
	}

	public String getAddDes() {
		return addDes;
	}

	public String getDelDes() {
		return delDes;
	}

	public String getGiveBeer() {
		return giveBeer;
	}

	public String getTakeBeer() {
		return takeBeer;
	}

	public String getTheElk() {
		return theElk;
	}

	public String getPingPong() {
		return pingPong;
	}

	public String getWaterfall() {
		return waterfall;
	}

	public String getHeyJack() {
		return heyJack;
	}

	public String getHelloQueen() {
		return helloQueen;
	}

	public String getBrands() {
		return brands;
	}

	public String getAddRule() {
		return addRule;
	}

	public String getDeleteRule() {
		return deleteRule;
	}
	
}
