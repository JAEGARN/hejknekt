package com.example.TheGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.util.Log;

public class GameEngine {
	private List<String> cards;
	private List<String> cardsDes;
	private Random random;
	private int theCurrentNumber;
	private CardMissions cm;
	
	public GameEngine(){
		cm = new CardMissions();
		cards = new ArrayList<String>();
		cardsDes = new ArrayList<String>();
		addCards();
		addDescription();
	}
	
	public void addCards(){
		for(int i = 0; i <= 4; i++){
			cards.add(cm.getBrands());
			cards.add(cm.getAddRule());
			cards.add(cm.getDeleteRule());
			cards.add(cm.getGiveBeer() + " ("+(i+1)+")");
			cards.add(cm.getHelloQueen());
			cards.add(cm.getHeyJack());
			cards.add(cm.getPingPong());
			cards.add(cm.getTakeBeer() + " ("+(i+1)+")");
			cards.add(cm.getTheElk());
			cards.add(cm.getWaterfall());
		}
		Log.d("hej", cards.size()+"card size");
	}
	
	public void addDescription(){
		cardsDes.add(cm.getAddDes());
		cardsDes.add(cm.getBrandsDes());
		cardsDes.add(cm.getDelDes());
		cardsDes.add(cm.getElkDes());
		cardsDes.add(cm.getGiveDes());
		cardsDes.add(cm.getJackDes());
		cardsDes.add(cm.getPingDes());
		cardsDes.add(cm.getQueenDes());
		cardsDes.add(cm.getTakeDes());
		cardsDes.add(cm.getWaterDes());
	}
	
	public String drawCard(){
		random = new Random();
		theCurrentNumber = random.nextInt(cards.size());
		return (String) cards.get(theCurrentNumber);
	}
	
	public List<String> getCards(){
		return cards;
	}
	public int getTheCurrentNumber(){
		return theCurrentNumber;
	}
}
